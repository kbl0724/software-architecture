import classes.AerospaceEngineer;
import classes.Airplane;
import classes.AirplaneBuilder;
import classes.builders.Airliner;
import classes.builders.CropDuster;

public class Main {
    public static void main(String[] args) {
        AerospaceEngineer engineer = new AerospaceEngineer();

        AirplaneBuilder crop = new CropDuster("Farmer Joe");
        AirplaneBuilder airliner = new Airliner("United Airlines");

        // Construct crop airplane.
        engineer.setAirplaneBuilder(crop);
        engineer.constructAirplane();
        Airplane completedCropDuster = engineer.getAirplane();
        System.out.println(completedCropDuster.getType() + " is completed and ready for delivery to " + completedCropDuster.getCustomer());

        // Construct airliner airplane.
        engineer.setAirplaneBuilder(airliner);
        engineer.constructAirplane();
        Airplane completedAirLiner = engineer.getAirplane();
        System.out.println(completedAirLiner.getType() + " is completed and ready for delivery to " + completedAirLiner.getCustomer());
    }
}