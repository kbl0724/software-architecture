import classes.ChicagoPizzaStore;
import classes.NYPizzaStore;

public class Main {
    public static void main(String[] args) {

        // Make a cheese pizza from the NewYork pizza store.
        NYPizzaStore nyPizzaStore = new NYPizzaStore();
        nyPizzaStore.orderPizza("cheese");

        System.out.println("\n---------------------------------------------\n");

        // Make a cheese pizza from the Chicago pizza store.
        ChicagoPizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
        chicagoPizzaStore.orderPizza("cheese");
    }
}