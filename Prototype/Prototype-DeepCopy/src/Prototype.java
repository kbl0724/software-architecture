public class Prototype implements Cloneable{
    private int x;
    private Nested ref = new Nested();

    public Prototype clone() {
        Prototype prototype = new Prototype();
        prototype.x = this.x;
        prototype.ref = this.ref.clone();
        return prototype;
    }
}
