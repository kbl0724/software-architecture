package classes.pizzas;

public class NYStyleClamPizza extends Pizza {
    public NYStyleClamPizza() {
        this.name = "뉴욕 스타일 조개 피자";
        this.dough = "일반 도우";
        this.sauce = "토마토 소스";
        this.toppings.add("조개");
        this.toppings.add("새우");
    }
}
