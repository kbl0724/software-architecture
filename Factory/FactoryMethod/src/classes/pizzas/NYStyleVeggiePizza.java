package classes.pizzas;

public class NYStyleVeggiePizza extends Pizza {
    public NYStyleVeggiePizza() {
        this.name = "뉴욕 스타일 재식 피자";
        this.dough = "일반 도우";
        this.sauce = "양파 소스";
        this.toppings.add("시금치");
        this.toppings.add("고구마");
    }
}
