package classes;

import interfaces.Iterator;

public class Waitress {
    PanCakeHouseMenu panCakeHouseMenu;
    DinnerMenu dinnerMenu;

    public Waitress(PanCakeHouseMenu panCakeHouseMenu, DinnerMenu dinnerMenu) {
        this.panCakeHouseMenu = panCakeHouseMenu;
        this.dinnerMenu = dinnerMenu;
    }

    public void printMenu() {
        Iterator panCakeIterator = panCakeHouseMenu.createIterator();
        Iterator dinnerIterator = dinnerMenu.createIterator();

        System.out.println("메뉴\n-----\n아침 메뉴");
        printMenu(panCakeIterator);
        System.out.println("\n점심 메뉴");
        printMenu(dinnerIterator);
    }

    private void printMenu(Iterator iterator) {
        while (iterator.hasNext()) {
            MenuItem menuItem = iterator.next();
            System.out.print(menuItem.getName() + ", ");
            System.out.print(menuItem.getPrice() + " -- ");
            System.out.println(menuItem.getDescription());
        }
    }
}
