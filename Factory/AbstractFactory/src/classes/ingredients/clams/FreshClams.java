package classes.ingredients.clams;

import classes.ingredients.clams.Clams;

public class FreshClams extends Clams {
    public FreshClams() {
        this.name = "Fresh Clam";
    }
}
