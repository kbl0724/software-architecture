package classes.pizzas;

public class CheesePizza extends Pizza {
    public CheesePizza() {
        this.name = "치즈 피자";
        this.dough = "일반 도우";
        this.sauce = "토마토 소스";
        this.toppings.add("베이컨");
        this.toppings.add("감자");
    }
}
