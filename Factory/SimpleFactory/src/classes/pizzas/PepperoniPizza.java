package classes.pizzas;

public class PepperoniPizza extends Pizza {
    public PepperoniPizza() {
        this.name = "페페로니 피자";
        this.dough = "일반 도우";
        this.sauce = "토마토 소스";
        this.toppings.add("불고기");
        this.toppings.add("토마토");
    }
}
