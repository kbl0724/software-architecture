import classes.MallarDuck;
import classes.TurkeyAdapter;
import classes.WildTurkey;
import interfaces.Duck;
import interfaces.Turkey;

public class Main {
    public static void main(String[] args) {
        Duck mallarDuck = new MallarDuck();
        testDuck(mallarDuck);

        System.out.println("\n------------------------------------------\n");

        Turkey wildTurkey = new WildTurkey();
        Duck adapter = new TurkeyAdapter(wildTurkey);
        testDuck(adapter);

    }

    static void testDuck(Duck duck) {
        duck.quack();
        duck.fly();
    }
}