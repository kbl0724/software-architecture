package classes.ingredients.dough;

import classes.ingredients.dough.Dough;

public class ThinCrustDough extends Dough {
    public ThinCrustDough() {
        this.name = "Thin Crust Dough";
    }
}
