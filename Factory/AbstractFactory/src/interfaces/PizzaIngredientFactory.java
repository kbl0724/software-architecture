package interfaces;

import classes.ingredients.cheese.Cheese;
import classes.ingredients.clams.Clams;
import classes.ingredients.dough.Dough;
import classes.ingredients.pepperoni.Pepperoni;
import classes.ingredients.sauce.Sauce;
import classes.ingredients.veggies.Veggies;

public interface PizzaIngredientFactory {
    public Dough createDough();
    public Sauce createSauce();
    public Cheese createCheese();
    public Veggies[] createVeggies();
    public Pepperoni createPepperoni();
    public Clams createClams();
}
