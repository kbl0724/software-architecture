package classes.builders;

import classes.Airplane;
import classes.AirplaneBuilder;

public class Airliner extends AirplaneBuilder {

    public Airliner(String customer) {
        this.customer = customer;
        this.type = "787 DreamLiner";
    }

    @Override
    public void buildWings() {
        airplane.setWingspan(197f);
    }

    @Override
    public void buildPowerPlant() {
        airplane.setPowerPlant("dual turbofan");
    }

    @Override
    public void buildAvionics() {
        airplane.setAvionics("commercial");
    }

    @Override
    public void buildSeats() {
        airplane.setNumberSeats(8, 289);
    }
}
