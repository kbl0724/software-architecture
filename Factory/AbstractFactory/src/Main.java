import classes.NYPizzaStore;
import classes.PizzaStore;
import classes.pizzas.Pizza;

public class Main {
    public static void main(String[] args) {
        PizzaStore nyPizzaStore = new NYPizzaStore();
        Pizza pizza = nyPizzaStore.orderPizza("cheese");
        System.out.println("\nPizza Name : " + pizza.getName());
    }
}