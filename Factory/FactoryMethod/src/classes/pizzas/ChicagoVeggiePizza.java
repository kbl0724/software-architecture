package classes.pizzas;

public class ChicagoVeggiePizza extends Pizza {
    public ChicagoVeggiePizza() {
        this.name = "재식 피자";
        this.dough = "일반 도우";
        this.sauce = "양파 소스";
        this.toppings.add("시금치");
        this.toppings.add("고구마");
    }
}
