package interfaces;

import classes.MenuItem;

public interface Iterator {
    boolean hasNext();
    MenuItem next();
}
