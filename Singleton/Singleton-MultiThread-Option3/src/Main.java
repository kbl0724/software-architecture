public class Main {
    public static void main(String[] args) {

        Singleton s1 = Singleton.getInstance();
        Singleton s2 = Singleton.getInstance();

        // Run on the multi thread environment to check exactly.
        System.out.println(s1 == s2);
    }
}