package classes;

public class AerospaceEngineer {
    private AirplaneBuilder airplaneBuilder;

    public void setAirplaneBuilder(AirplaneBuilder airplaneBuilder) {
        this.airplaneBuilder = airplaneBuilder;
    }

    public Airplane getAirplane() {
        return airplaneBuilder.getAirplane();
    }

    public void constructAirplane() {
        airplaneBuilder.createNewAirPlane();
        airplaneBuilder.buildWings();
        airplaneBuilder.buildPowerPlant();
        airplaneBuilder.buildAvionics();
        airplaneBuilder.buildSeats();
    }
}