package classes.ingredients.cheese;

import classes.ingredients.cheese.Cheese;

public class ReggianoCheese extends Cheese {
    public ReggianoCheese() {
        this.name = "Reggiano Cheese";
    }
}
