import classes.PizzaStore;
import classes.SimplePizzaFactory;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        SimplePizzaFactory factory = new SimplePizzaFactory();
        PizzaStore store = new PizzaStore(factory);

        // Make a veggie pizza.
        store.orderPizza("veggie");

        System.out.println("\n-----------------------------------------\n");

        // Make a cheese pizza.
        store.orderPizza("clam");

    }
}