import classes.Beverage;
import classes.beverages.HouseBlend;
import classes.condiments.Mocha;
import classes.condiments.Whip;

public class Main {
    public static void main(String[] args) {
        Beverage beverage1 = new HouseBlend();
        System.out.println(beverage1.getDescription() + " / $" + beverage1.cost());

        Beverage beverage2 = new HouseBlend();
        beverage2 = new Whip(beverage2);
        beverage2 = new Mocha(beverage2);
        System.out.println(beverage2.getDescription() + " / $" + beverage2.cost());
    }
}