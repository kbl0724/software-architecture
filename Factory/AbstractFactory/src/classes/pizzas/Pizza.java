package classes.pizzas;

import classes.ingredients.cheese.Cheese;
import classes.ingredients.clams.Clams;
import classes.ingredients.dough.Dough;
import classes.ingredients.pepperoni.Pepperoni;
import classes.ingredients.sauce.Sauce;
import classes.ingredients.veggies.Veggies;

public abstract class Pizza {
    String name;

    Dough dough;
    Sauce sauce;
    Veggies veggies[];
    Cheese cheese;
    Pepperoni pepperoni;
    Clams clam;

    public abstract void prepare();

    public void bake() {
        System.out.println("175도에서 25분간 굽기");
    }

    public void cut() {
        System.out.println("피자를 사선으로 자르기");
    }

    public void box() {
        System.out.println("상자에 피자 담기");
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
