package classes;

import interfaces.Duck;

public class MallarDuck implements Duck {

    @Override
    public void quack() {
        System.out.println("꽥");
    }

    @Override
    public void fly() {
        System.out.println("날고 있어요!!");
    }
}
