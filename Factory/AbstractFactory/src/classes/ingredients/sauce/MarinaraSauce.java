package classes.ingredients.sauce;

public class MarinaraSauce extends Sauce {
    public MarinaraSauce() {
        this.name = "Marinara sauce";
    }
}
