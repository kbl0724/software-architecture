package classes.ingredients.pepperoni;

import classes.ingredients.pepperoni.Pepperoni;

public class SlicedPepperoni extends Pepperoni {
    public SlicedPepperoni() {
        this.name = "Pepperoni";
    }
}
