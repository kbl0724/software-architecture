import classes.ConcreteHandler1;
import classes.ConcreteHandler2;
import classes.ConcreteHandler3;
import classes.Handler;

public class Main {
    public static void main(String[] args) {
        int[] requests = {2, 5, 14, 22, 18, 3, 27, 20};
        
        Handler handler1 = new ConcreteHandler1();
        Handler handler2 = new ConcreteHandler2();
        Handler handler3 = new ConcreteHandler3();
        
//        handler1.setSuccessor(handler2);
//        handler2.setSuccessor(handler3);

        handler1.setSuccessor(handler2);
        handler1.HandleRequest(13);

//        for (int request: requests) {
//            handler1.HandleRequest(request);
//        }
    }
}