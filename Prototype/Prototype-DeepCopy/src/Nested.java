public class Nested {
    int data;

    public void op() {
        System.out.println("Do some operating...");
    }

    public Nested clone() {
        Nested nested = new Nested();
        nested.data = this.data;
        return nested;
    }
}
