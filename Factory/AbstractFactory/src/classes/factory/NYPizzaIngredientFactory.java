package classes.factory;

import classes.ingredients.cheese.Cheese;
import classes.ingredients.cheese.ReggianoCheese;
import classes.ingredients.clams.Clams;
import classes.ingredients.clams.FreshClams;
import classes.ingredients.dough.Dough;
import classes.ingredients.dough.ThinCrustDough;
import classes.ingredients.pepperoni.Pepperoni;
import classes.ingredients.pepperoni.SlicedPepperoni;
import classes.ingredients.sauce.MarinaraSauce;
import classes.ingredients.sauce.Sauce;
import classes.ingredients.veggies.*;
import interfaces.PizzaIngredientFactory;

public class NYPizzaIngredientFactory implements PizzaIngredientFactory {
    @Override
    public Dough createDough() {
        return new ThinCrustDough();
    }

    @Override
    public Sauce createSauce() {
        return new MarinaraSauce();
    }

    @Override
    public Cheese createCheese() {
        return new ReggianoCheese();
    }

    @Override
    public Veggies[] createVeggies() {
        Veggies veggies[] = {new Garlic(), new Onion(), new Mushroom(), new RedPepper()};
        return veggies;
    }

    @Override
    public Pepperoni createPepperoni() {
        return new SlicedPepperoni();
    }

    @Override
    public Clams createClams() {
        return new FreshClams();
    }
}
